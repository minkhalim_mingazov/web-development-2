package com.example.twitter.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {

    private static int score=0;

    @RequestMapping({"/","index"})
    public String index(Model model, @RequestParam(name="action", required = false, defaultValue = "") String action) {
        if(action.equals("add"))
            score++;
        if(action.equals("subtract"))
            score--;
        if(action.equals("reset"))
            score=0;
        model.addAttribute("scoreText","Score: "+Integer.toString(score));
        return "index";
    }

}