package com.example.logging.controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class MainController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    @ResponseBody
    @RequestMapping(path = "/")
    public String home() {

        LOGGER.trace("Это TRACE");
        LOGGER.debug("Это DEBUG");
        LOGGER.info("Это INFO");
        LOGGER.warn("Это WARN");
        LOGGER.error("Это ERROR");

        return "Привет, логи отображены в консоли или файле!";
    }

    @ResponseBody
    @RequestMapping(path = "/errorAndTrace")
    public String errorAndTrace() {

        LOGGER.trace("Это TRACE");
        LOGGER.error("Это ERROR");

        return "Привет, TRACE и ERROR отображены в консоли или файле!";
    }

}