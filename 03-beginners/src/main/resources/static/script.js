class Cell {
    constructor(isCellLight) {
        this.isCellLight = isCellLight;
        this.isChecker = false;
        this.isQueen = false;
        this.isWhite = false;
        this.isMarked = false;
    }

    copy() {
        var cell = new Cell(this.isCellLight);
        cell.isChecker = this.isChecker;
        cell.isQueen = this.isQueen;
        cell.isWhite = this.isWhite;
        cell.isMarked = this.isMarked;
        return cell;
    }
}

class Board {
    constructor(init = false) {
        this.board = new Array();
        for (var i = 0; i < 8; i++) {
            this.board[i] = new Array();
            if (init)
                for (var j = 0; j < 8; j++)
                    this.board[i][j] = new Cell((i + j) % 2 == 0);
        }
    }

    copy() {
        var newBoard = new Board();
        for (var i = 0; i < 8; i++)
            for (var j = 0; j < 8; j++)
                newBoard.board[i][j] = this.board[i][j].copy();
        return newBoard;
    }
}

class Move {
    constructor(fromI, fromJ, toI, toJ) {
        this.fromI = fromI;
        this.fromJ = fromJ;
        this.toI = toI;
        this.toJ = toJ;
    }
}

moves = new Set();

function resetBoard(updateLog = true) {
    isACellActive = false;
    isAttack = false;
    isMiddleOfTurn = false;
    turnPlayer = true;
    currentTurn = "";
    turnNumber = 1;
    board = new Board(true);
    if (updateLog)
        document.getElementById("log").value = "";
}

function cellToString(i, j) {
    return ["a", "b", "c", "d", "e", "f", "g", "h"][j] + (8 - i);
}

function stringToMove(s) {
    if ("abcdefgh".indexOf(s[0]) == -1 || "abcdefgh".indexOf(s[3]) == -1 || "12345678".indexOf(s[1]) == -1 || "12345678".indexOf(s[4]) == -1 || ":-".indexOf(s[2]) == -1)
        return null;
    return new Move(8 - s[1], {
        a: 0,
        b: 1,
        c: 2,
        d: 3,
        e: 4,
        f: 5,
        g: 6,
        h: 7
    }[s[0]], 8 - s[4], {
        a: 0,
        b: 1,
        c: 2,
        d: 3,
        e: 4,
        f: 5,
        g: 6,
        h: 7
    }[s[3]]);
}

function recalculate() {
    if (!isAttack && isMiddleOfTurn) {
        moves.clear();
        return;
    }
    var nonAttacks = new Set();
    var attacks = new Set();

    function addMoves(i, j) {
        var checker = board.board[i][j];
        if (checker.isChecker && checker.isWhite == turnPlayer) {
            if (checker.isQueen) {
                [-1, 1].forEach(function(stepI) {
                    [-1, 1].forEach(function(stepJ) {
                        var iterI = i + stepI;
                        var iterJ = j + stepJ;
                        var sawAnEnemy = false;
                        while (iterI >= 0 && iterJ >= 0 && iterI < 8 && iterJ < 8) {
                            if (!board.board[iterI][iterJ].isChecker) {
                                if (sawAnEnemy)
                                    attacks.add(new Move(i, j, iterI, iterJ));
                                else
                                    nonAttacks.add(new Move(i, j, iterI, iterJ));
                            } else if (board.board[iterI][iterJ].isWhite != checker.isWhite &&
                                !board.board[iterI][iterJ].isMarked) {
                                if (sawAnEnemy)
                                    break;
                                sawAnEnemy = true;
                            } else break;
                            iterI += stepI;
                            iterJ += stepJ;
                        }
                    })
                });
            } else {
                [-1, 1].forEach(function(stepI) {
                    [-1, 1].forEach(function(stepJ) {
                        if (i + stepI < 0 || j + stepJ < 0 || i + stepI > 7 || j + stepJ > 7)
                            return;
                        if (!board.board[i + stepI][j + stepJ].isChecker &&
                            stepI == (turnPlayer ? -1 : 1))
                            nonAttacks.add(new Move(i, j, i + stepI, j + stepJ));
                        if (i + stepI * 2 < 0 || j + stepJ * 2 < 0 || i + stepI * 2 > 7 || j + stepJ * 2 > 7)
                            return;
                        if (board.board[i + stepI][j + stepJ].isChecker &&
                            board.board[i + stepI][j + stepJ].isWhite != checker.isWhite &&
                            !board.board[i + stepI][j + stepJ].isMarked &&
                            !board.board[i + stepI * 2][j + stepJ * 2].isChecker)
                            attacks.add(new Move(i, j, i + stepI * 2, j + stepJ * 2));
                    })
                });
            }
        }
    };
    if (isMiddleOfTurn)
        addMoves(activeI, activeJ);
    else
        for (var i = 0; i < 8; i++)
            for (var j = 0; j < 8; j++)
                addMoves(i, j);
    if (attacks.size > 0)
        isAttack = true;
    if (isAttack)
        moves = attacks;
    else
        moves = nonAttacks;
}

function redraw() {
    recalculate();
    var boardElement = document.getElementById("board").children[1];
    var validMoveTargets = new Set();
    if (isACellActive) {
        moves.forEach(function(action) {
            if (action.fromI == activeI && action.fromJ == activeJ)
                validMoveTargets.add(cellToString(action.toI, action.toJ));
        });
    }
    for (var i = 0; i < 8; i++)
        for (var j = 0; j < 8; j++) {
            var cell = board.board[i][j];
            var classes = boardElement.children[i + 1].children[j + 1].classList;
            classes.value = cell.isCellLight ? "cell light" : "cell dark";
            if (cell.isChecker) {
                classes.add("checker");
                classes.add(cell.isWhite ? "white" : "black");
                if (cell.isQueen)
                    classes.add("queen");
                if (cell.isMarked)
                    classes.add("marked");
            }
            if (isACellActive) {
                if (i == activeI && j == activeJ)
                    classes.add("active");
                else if (validMoveTargets.has(cellToString(i, j))) {
                    classes.add(isAttack ? "attack" : "move");
                    classes.add("target");
                }
            }
        }
    document.getElementById("board").children[0].textContent = "Ход " + (turnPlayer ? "белых" : "чёрных");
    document.getElementById("cancelButton").disabled = !isMiddleOfTurn;
    document.getElementById("submitButton").disabled = moves.size > 0 || !isMiddleOfTurn;
}

function performMove(move) {
    console.log(move);
    if (!isMiddleOfTurn) {
        backupBoard = board.copy();
        isMiddleOfTurn = true;
    }

    temp = board.board[move.toI][move.toJ];
    board.board[move.toI][move.toJ] = board.board[move.fromI][move.fromJ];
    board.board[move.fromI][move.fromJ] = temp;

    var signI = move.toI > move.fromI ? 1 : -1;
    var signJ = move.toJ > move.fromJ ? 1 : -1;

    for (var dist = 1; move.fromI + signI * dist != move.toI; dist++) {
        if (board.board[move.fromI + signI * dist][move.fromJ + signJ * dist].isChecker)
            board.board[move.fromI + signI * dist][move.fromJ + signJ * dist].isMarked = true;
    }

    var delimiter = isAttack ? ":" : "-";
    if (currentTurn == "")
        currentTurn = cellToString(move.fromI, move.fromJ);
    currentTurn += delimiter + cellToString(move.toI, move.toJ);

    activeI = move.toI;
    activeJ = move.toJ;

    redraw();
}

function cancel() {
    if (!isMiddleOfTurn)
        return false;
    isMiddleOfTurn = false;
    isACellActive = false;
    isAttack = false;
    currentTurn = "";
    board = backupBoard;

    redraw();
    return true;
}

function submit(updateLog = true) {
    if (moves.size > 0 || !isMiddleOfTurn)
        return false;
    for (var i = 0; i < 8; i++)
        for (var j = 0; j < 8; j++)
            if (board.board[i][j].isMarked)
                board.board[i][j].isChecker = false;

    for (var j = 0; j < 8; j++) {
        if (board.board[0][j].isChecker && board.board[0][j].isWhite)
            board.board[0][j].isQueen = true;
        if (board.board[7][j].isChecker && !board.board[7][j].isWhite)
            board.board[7][j].isQueen = true;
    }

    if (updateLog)
        if (turnPlayer)
            document.getElementById("log").value += turnNumber + ". " + currentTurn;
        else
            document.getElementById("log").value += " " + currentTurn + "\n";
		
	if(!turnPlayer)
        turnNumber++;

    turnPlayer = !turnPlayer;
    isMiddleOfTurn = false;
    isACellActive = false;
    isAttack = false;
    currentTurn = "";
    redraw();
    return true;
}

function loadStartPosition(updateLog = true) {
    resetBoard(updateLog);
    for (var i = 0; i < 3; i++)
        for (var j = (i + 1) % 2; j < 8; j += 2) {
            board.board[i][j].isChecker = true;
        }
    for (var i = 5; i < 8; i++)
        for (var j = (i + 1) % 2; j < 8; j += 2) {
            board.board[i][j].isChecker = true;
            board.board[i][j].isWhite = true;
        }
    redraw();
}

function loadExample1() {
    resetBoard();
    board.board[4][5].isChecker = true;
    board.board[4][5].isWhite = true;
    board.board[4][7].isChecker = true;
    board.board[4][7].isWhite = true;
    board.board[0][1].isChecker = true;
    board.board[7][2].isChecker = true;
    board.board[7][2].isQueen = true;
    board.board[3][2].isChecker = true;
    board.board[1][2].isChecker = true;
    board.board[1][4].isChecker = true;
    board.board[2][7].isChecker = true;
    redraw();
}

function moveFromNotation(str, start) {
    if ((str[start + 2] == "-") == isAttack)
        return -1; //-1 - маркировка -/: не соответствует тому, является ли ход атакой
    var iterStep = stringToMove(str.substring(start, start + 5));
    stepIsValid = false;
    moves.forEach(function(move) {
        if (move.fromI == iterStep.fromI &&
            move.fromJ == iterStep.fromJ &&
            move.toI == iterStep.toI &&
            move.toJ == iterStep.toJ)
            stepIsValid = true;
    });
    if (!stepIsValid) {
        stepIsValid = undefined;
        return -2; //-2 - ход некорректен по иной причине
    }
    stepIsValid = undefined;
    return iterStep;
}

function show() {
    loadStartPosition(false);
    var text = document.getElementById("log").value.toLowerCase();
    var regexOutput;
    var turn = 1;
    while ((regexOutput = /[a-h][1-8]([:-][a-h][1-8])+/.exec(text)) != null) {
        var moveText = regexOutput[0];
        var moveIsValid = true;
        if (moveText.length > 5 && moveText.indexOf("-") != -1) {
            alert("Ход " + turn + (turnPlayer ? " белых" : " чёрных") + ", некорректный идентификатор хода (дефис в ходе из нескольких действий)!");
            moveIsValid = false;
        } else
            for (var i = 0; i <= moveText.length - 5 && moveIsValid; i += 3) {
                var move = moveFromNotation(moveText, i);
                switch (move) {
                    case -1:
                        alert("Ход " + turn + (turnPlayer ? " белых" : " чёрных") + ", некорректный идентификатор хода (" + (isAttack ? "обычный ход обозначен двоиточием" : "взятие обозначено дефисом") + ")!");
                        moveIsValid = false;
                        break;
                    case -2:
                        alert("Ход " + turn + (turnPlayer ? " белых" : " чёрных") + ", некорректный идентификатор хода (действие невозможно)!");
                        moveIsValid = false;
                        break;
                    default:
                        performMove(move);
                        break;
                }
            }

        if (!moveIsValid)
            break;

        if (!submit(false)) {
            alert("Ход" + turn + (turnPlayer ? "белых" : "чёрных") + ", ход не может быть закончен!");
            break;
        } else if (turnPlayer)
            turn++;

        text = text.substring(regexOutput[0].length + regexOutput.index);
    }
    redraw();
}

loadStartPosition();

document.getElementById("startPositionButton").onclick = loadStartPosition;
document.getElementById("example1Button").onclick = loadExample1;

document.getElementById("cancelButton").onclick = cancel;
document.getElementById("submitButton").onclick = submit;

document.getElementById("showButton").onclick = show;

for (var i = 0; i < 8; i++)
    for (var j = 0; j < 8; j++) {
        var elem = document.getElementById("board").children[1].children[i + 1].children[j + 1];
        elem.i = i;
        elem.j = j;
        elem.onclick = function() {
            if (!isACellActive) {
                if (!board.board[this.i][this.j].isChecker || board.board[this.i][this.j].isWhite != turnPlayer)
                    return;
                isACellActive = true;
                activeI = this.i;
                activeJ = this.j;
            } else if (activeI == this.i && activeJ == this.j && !isMiddleOfTurn)
                isACellActive = false;
            else {
                var i = this.i;
                var j = this.j;
                moves.forEach(function(move) {
                    if (move.fromI == activeI && move.fromJ == activeJ && move.toI == i && move.toJ == j)
                        performMove(move)
                });
            }

            redraw();
        }
    }