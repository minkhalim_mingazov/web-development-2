package com.example.mustache.dao;

import com.example.mustache.model.Employee;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Repository
public class EmployeeDAO {

    public List<Employee> getEmployees()  {

        Date hireDate1= Date.valueOf(LocalDate.parse("1970-01-01"));
        Employee e1= new Employee(0L, "A01", "Мингазов Минхалим", hireDate1);

        Date hireDate2= Date.valueOf(LocalDate.parse("2001-12-21"));
        Employee e2= new Employee(1L, "E01", "Раз", hireDate2);

        Date hireDate3= Date.valueOf(LocalDate.parse("2003-04-03"));
        Employee e3= new Employee(2L, "E02", "Два", hireDate3);

        List<Employee> list= new ArrayList<Employee>();
        list.add(e1);
        list.add(e2);
        list.add(e3);
        return list;
    }

}