package com.example.mustache.controller;

import java.util.List;
import java.util.stream.Collectors;

import com.example.mustache.dao.EmployeeDAO;
import com.example.mustache.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

    @Autowired
    private EmployeeDAO employeeDAO;


    @RequestMapping("/")
    public String handleRequest(Model model) {

        List<Employee> employees = employeeDAO.getEmployees();
        model.addAttribute("employees", employees);
        return "employee";
    }

    @RequestMapping("/admins")
    public String handleAdminListRequest(Model model) {

        List<Employee> employees = employeeDAO.getEmployees().stream().filter(employee -> employee.getEmpNo().charAt(0) == 'A').collect(Collectors.toList());
        model.addAttribute("employees", employees);
        return "employee";
    }
}