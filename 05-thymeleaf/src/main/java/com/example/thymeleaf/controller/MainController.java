package com.example.thymeleaf.controller;

import java.util.ArrayList;
import java.util.List;

import com.example.thymeleaf.form.PersonForm;
import com.example.thymeleaf.model.Person;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {
    private static List<Person> people = new ArrayList<>();

    static {
        people.add(new Person("Мингазов", "Минхалим", "Рафаэлевич"));
        people.add(new Person("Ашаев", "Игорь", "Викторович"));
    }

    @Value("${message.welcome}")
    private String message;

    @Value("${message.error}")
    private String errorMessage;

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String index(Model model) {

        model.addAttribute("message", message);

        return "index";
    }

    @RequestMapping(value = {"/personList"}, method = RequestMethod.GET)
    public String personList(Model model) {

        model.addAttribute("count",people.size());
        model.addAttribute("persons", people);

        return "personList";
    }

    @RequestMapping(value = {"/personList/noCount"}, method = RequestMethod.GET)
    public String personListNoCount(Model model) {

        model.addAttribute("persons", people);

        return "personList";
    }

    @RequestMapping(value = {"/addPerson"}, method = RequestMethod.GET)
    public String showAddPersonPage(Model model) {

        PersonForm personForm = new PersonForm();
        model.addAttribute("personForm", personForm);

        return "addPerson";
    }

    @RequestMapping(value = {"/addPerson"}, method = RequestMethod.POST)
    public String savePerson(Model model,
                             @ModelAttribute("personForm") PersonForm personForm) {

        String firstName = personForm.getFirstName();
        String lastName = personForm.getLastName();
        String patronymic = personForm.getPatronymic();

        if (lastName != null && lastName.length() > 0 //
                && firstName != null && firstName.length() > 0
                && patronymic != null && patronymic.length() > 0) {
            Person newPerson = new Person(lastName, firstName, patronymic);
            people.add(newPerson);

            return "redirect:/personList";
        }

        model.addAttribute("errorMessage", errorMessage);
        return "addPerson";
    }

}
