package com.example.logging.controller;
import com.example.logging.schedule.TickCounterSchedule;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class MainController {

    @ResponseBody
    @RequestMapping(path = "/")
    public String home() {
        return "В течение этой минуты прошло "+ TickCounterSchedule.getTicks() +" тиков!";
    }

}