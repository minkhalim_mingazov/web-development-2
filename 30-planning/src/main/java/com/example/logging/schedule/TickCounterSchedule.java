package com.example.logging.schedule;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TickCounterSchedule {

    private static int ticks = 0;

    public static int getTicks() {
        return ticks;
    }

    @Scheduled(initialDelay = 400, fixedDelay = 400)
    public void addATick() {
        ticks++;
    }

    //Технически неверно
    @Scheduled(cron="0 * * * * *", zone="Asia/Novosibirsk")
    public void resetTicks() {
        System.out.println("За минуту прошло "+ticks+" тиков! Сбрасываем счётчик!");
        ticks=0;
    }
}
